'use strict';

/**
 * @ngdoc function
 * @name claseAngularApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the claseAngularApp
 */
angular.module('claseAngularApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
